﻿using System;

namespace Trucker.API.Domain.DTOs
{
    public class PositionDto
    {
        public Guid Id { get; set; }
        public Guid TruckPlanId { get; set; }
        public DateTimeOffset Timestamp { get; set; }
        public decimal Longitude { get; set; }
        public decimal Latitude { get; set; }
        public string Continent { get; set; }
        public string Country { get; set; }

        public TruckPlanDto TruckPlan { get; set; }
    }
}
