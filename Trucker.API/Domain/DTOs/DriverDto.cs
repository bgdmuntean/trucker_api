﻿using System;
using System.Collections.Generic;

namespace Trucker.API.Domain.DTOs
{
    public sealed class DriverDto
    {
        public DriverDto()
        {
            TruckPlans = new HashSet<TruckPlanDto>();
        }

        public Guid Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTimeOffset Birthdate { get; set; }
        public string DriversLicenseNo { get; set; }
        public string Nationality { get; set; }
        public DateTimeOffset CreatedAt { get; set; }
        public DateTimeOffset ModifiedAt { get; set; }

        public ICollection<TruckPlanDto> TruckPlans { get; set; }
    }
}
