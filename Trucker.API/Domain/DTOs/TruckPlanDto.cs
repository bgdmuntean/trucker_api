﻿using System;
using System.Collections.Generic;

namespace Trucker.API.Domain.DTOs
{
    public class TruckPlanDto
    {
        public Guid Id { get; set; }
        public Guid DriverId { get; set; }
        public string Start { get; set; }
        public string Finish { get; set; }
        public decimal Kilometers { get; set; }
        public decimal Duration { get; set; }

        public DriverDto Driver{ get; set; }
        public ICollection<PositionDto> Positions { get; set; }
    }
}
