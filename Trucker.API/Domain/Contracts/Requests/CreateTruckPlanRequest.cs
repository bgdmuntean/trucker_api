﻿using System;
using System.Collections.Generic;

namespace Trucker.API.Domain.Contracts.Requests
{
    public class CreateTruckPlanRequest
    {
        public Guid DriverId { get; set; }
        public decimal StartLongitude { get; set; }
        public decimal StartLatitude { get; set; }
        public decimal FinishLongitude { get; set; }
        public decimal FinishLatitude { get; set; }

        public ICollection<CreatePositionRequest> Positions { get; set; }
    }
}