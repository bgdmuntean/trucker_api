﻿using System;

namespace Trucker.API.Domain.Contracts.Requests
{
    public class CreatePositionRequest
    {
        public DateTime Timespan { get; set; }
        public decimal Longitude { get; set; }
        public decimal Latitude { get; set; }
    }
}