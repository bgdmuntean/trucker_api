﻿using System;

namespace Trucker.API.Domain.Contracts.Requests
{
    public class CreateDriverRequest
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime Birthdate { get; set; }
        public string Nationality { get; set; }
        public string DriversLicenseNo { get; set; }
    }
}