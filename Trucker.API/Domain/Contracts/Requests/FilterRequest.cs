﻿using System;

namespace Trucker.API.Domain.Contracts.Requests
{
    public class FilterRequest
    {
        public int DriveAgeOver { get; set; }
        public string Country { get; set; }
        public DateTime DateFrom { get; set; }
        public DateTime DateTo { get; set; }
    }
}