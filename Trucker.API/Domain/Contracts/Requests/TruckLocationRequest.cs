﻿namespace Trucker.API.Domain.Contracts.Requests
{
    public class TruckLocationRequest
    {
        public decimal Longitude { get; set; }
        public decimal Latitude { get; set; }
    }
}