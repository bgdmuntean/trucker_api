﻿using System;
using Trucker.API.Domain.Models.ValueObjects.Driver;

namespace Trucker.API.Domain.Contracts.Responses
{
    public class DriverResponse
    {
        public Guid Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Nationality { get; set; }
        public string DriversLicenseNo { get; set; }
        public DateTime Birthdate { get; set; }
    }
}