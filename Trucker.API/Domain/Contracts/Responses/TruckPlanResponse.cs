﻿using System;
using System.Collections.Generic;

namespace Trucker.API.Domain.Contracts.Responses
{
    public class TruckPlanResponse
    {
        public Guid Id { get; set; }
        public Guid DriverId { get; set; }
        public string Start { get; set; }
        public string Finish { get; set; }
        public decimal Kilometers { get; set; }
        public decimal Duration { get; set; }
        public ICollection<PositionResponse> Positions { get; set; } = new List<PositionResponse>();
    }
}