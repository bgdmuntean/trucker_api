﻿using System;

namespace Trucker.API.Domain.Contracts.Responses
{
    public class FilterResponse
    {
        public decimal Kilometers { get; set; }
        public string Country { get; set; }
        public DateTime DateFrom { get; set; }
        public DateTime DateTo { get; set; }
        public int AgeOver { get; set; }
    }
}