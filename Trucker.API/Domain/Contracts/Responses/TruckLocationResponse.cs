﻿namespace Trucker.API.Domain.Contracts.Responses
{
    public class TruckLocationResponse
    {
        public string Country { get; set; }
        public decimal Longitude { get; set; }
        public decimal Latitude { get; set; }
    }
}