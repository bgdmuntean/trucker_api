﻿using System;

namespace Trucker.API.Domain.Contracts.Responses
{
    public class PositionResponse
    {
        public Guid Id { get; set; }
        public Guid TruckPlanId { get; set; }
        public DateTimeOffset Timestamp { get; set; }
        public decimal Longitude { get; set; }
        public decimal Latitude { get; set; }
        public string Continent { get; set; }
        public string Country { get; set; }
    }
}