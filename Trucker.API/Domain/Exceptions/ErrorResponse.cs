﻿using System;
using System.Net;
using Newtonsoft.Json;

namespace Trucker.API.Domain.Exceptions
{
    internal class ErrorResponse
    {
        public ErrorResponse(Exception exception)
        {
            StatusCode = HttpStatusCode.InternalServerError;
            Message = string.IsNullOrWhiteSpace(exception.Message) ? JsonConvert.SerializeObject(exception) : exception.Message;
        }

        public ErrorResponse(ApiException exception)
        {
            StatusCode = exception.StatusCode;
            Message = exception.Message;
        }

        public HttpStatusCode StatusCode { get; set; }
        public string Message { get; set; }


        public string ToJsonString()
            => JsonConvert.SerializeObject(this);
    }
}