﻿using System;
using System.Net;

namespace Trucker.API.Domain.Exceptions
{
    public class DriverCreationException : ApiException
    {
        public DriverCreationException(HttpStatusCode statusCode, string message) : base(statusCode, message)
        {
        }

        public DriverCreationException(HttpStatusCode statusCode, string message, Exception ex) : base(statusCode, message, ex)
        {
        }
    }
}