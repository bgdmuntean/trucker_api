﻿using System;
using System.Net;

namespace Trucker.API.Domain.Exceptions.ValueObjectExceptions
{
    public class NullOrEmptyNationalityException : ApiException
    {
        public NullOrEmptyNationalityException(string message) : base(HttpStatusCode.BadRequest, message)
        {
        }

        public NullOrEmptyNationalityException(string message, Exception ex) : base(HttpStatusCode.BadRequest, message, ex)
        {
        }
    }
}