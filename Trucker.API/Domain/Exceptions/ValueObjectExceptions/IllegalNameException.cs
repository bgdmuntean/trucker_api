﻿using System;
using System.Net;

namespace Trucker.API.Domain.Exceptions.ValueObjectExceptions
{
    public class IllegalNameException : ApiException
    {
        public IllegalNameException(string message) : base(HttpStatusCode.BadRequest, message)
        {
        }

        public IllegalNameException(string message, Exception ex) : base(HttpStatusCode.BadRequest, message, ex)
        {
        }
    }
}
