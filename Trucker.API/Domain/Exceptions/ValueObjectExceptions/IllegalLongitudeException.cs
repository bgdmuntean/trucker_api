﻿using System;
using System.Net;

namespace Trucker.API.Domain.Exceptions.ValueObjectExceptions
{
    public class IllegalLongitudeException : ApiException
    {
        public IllegalLongitudeException(string message) : base(HttpStatusCode.BadRequest, message)
        {
        }

        public IllegalLongitudeException(string message, Exception ex) : base(HttpStatusCode.BadRequest, message, ex)
        {
        }
    }
}