﻿using System;
using System.Net;

namespace Trucker.API.Domain.Exceptions.ValueObjectExceptions
{
    public class MinimumDurationException : ApiException
    {
        public MinimumDurationException(string message) : base(HttpStatusCode.BadRequest, message)
        {
        }

        public MinimumDurationException(string message, Exception ex) : base(HttpStatusCode.BadRequest, message, ex)
        {
        }
    }
}