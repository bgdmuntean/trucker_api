﻿using System;
using System.Net;

namespace Trucker.API.Domain.Exceptions.ValueObjectExceptions
{
    public class MinimumDriverAgeException : ApiException
    {
        public MinimumDriverAgeException(string message) : base(HttpStatusCode.BadRequest, message)
        {
        }

        public MinimumDriverAgeException(string message, Exception ex) : base(HttpStatusCode.BadRequest, message, ex)
        {
        }
    }
}