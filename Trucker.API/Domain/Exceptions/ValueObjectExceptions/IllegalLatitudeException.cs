﻿using System;
using System.Net;

namespace Trucker.API.Domain.Exceptions.ValueObjectExceptions
{
    public class IllegalLatitudeException : ApiException
    {
        public IllegalLatitudeException(string message) : base(HttpStatusCode.BadRequest, message)
        {
        }

        public IllegalLatitudeException(string message, Exception ex) : base(HttpStatusCode.BadRequest, message, ex)
        {
        }
    }
}