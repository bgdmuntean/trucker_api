﻿using System;
using System.Net;

namespace Trucker.API.Domain.Exceptions.ValueObjectExceptions
{
    public class MinimumDistanceException : ApiException
    {
        public MinimumDistanceException(string message) : base(HttpStatusCode.BadRequest, message)
        {
        }

        public MinimumDistanceException(string message, Exception ex) : base(HttpStatusCode.BadRequest, message, ex)
        {
        }
    }
}