﻿using System;
using System.Net;

namespace Trucker.API.Domain.Exceptions.ValueObjectExceptions
{
    public class UnknownNationalityException : ApiException
    {
        public UnknownNationalityException(string message) : base(HttpStatusCode.BadRequest, message)
        {
        }

        public UnknownNationalityException(string message, Exception ex) : base(HttpStatusCode.BadRequest, message, ex)
        {
        }
    }
}