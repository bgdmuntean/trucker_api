﻿using System;
using System.Net;

namespace Trucker.API.Domain.Exceptions.ServiceExceptions
{
    public class DriverNotFoundException : ApiException
    {
        public DriverNotFoundException(string message) : base(HttpStatusCode.NotFound, message)
        {
        }

        public DriverNotFoundException(string message, Exception ex) : base(HttpStatusCode.NotFound, message, ex)
        {
        }
    }
}