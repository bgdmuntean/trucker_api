﻿using System;
using System.Net;

namespace Trucker.API.Domain.Exceptions.ServiceExceptions
{
    public class NoDriversOverAgeException : ApiException
    {
        public NoDriversOverAgeException(string message) : base(HttpStatusCode.NotFound, message)
        {
        }

        public NoDriversOverAgeException(string message, Exception ex) : base(HttpStatusCode.NotFound, message, ex)
        {
        }
    }
}