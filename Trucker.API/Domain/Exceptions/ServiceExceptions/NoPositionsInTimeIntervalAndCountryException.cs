﻿using System;
using System.Net;

namespace Trucker.API.Domain.Exceptions.ServiceExceptions
{
    public class NoPositionsInTimeIntervalAndCountryException : ApiException
    {
        public NoPositionsInTimeIntervalAndCountryException(string message) : base(HttpStatusCode.NotFound, message)
        {
        }

        public NoPositionsInTimeIntervalAndCountryException(string message, Exception ex) : base(HttpStatusCode.NotFound, message, ex)
        {
        }
    }
}