﻿using System;
using System.Net;

namespace Trucker.API.Domain.Exceptions.ServiceExceptions
{
    public class NoPositionsBelongingToTruckPlansFromCountryException : ApiException
    {
        public NoPositionsBelongingToTruckPlansFromCountryException(string message) : base(HttpStatusCode.NotFound, message)
        {
        }

        public NoPositionsBelongingToTruckPlansFromCountryException(string message, Exception ex) : base(HttpStatusCode.NotFound, message, ex)
        {
        }
    }
}