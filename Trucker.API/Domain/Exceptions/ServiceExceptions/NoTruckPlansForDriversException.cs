﻿using System;
using System.Net;

namespace Trucker.API.Domain.Exceptions.ServiceExceptions
{
    public class NoTruckPlansForDriversException : ApiException
    {
        public NoTruckPlansForDriversException(string message) : base(HttpStatusCode.NotFound, message)
        {
        }

        public NoTruckPlansForDriversException(string message, Exception ex) : base(HttpStatusCode.NotFound, message, ex)
        {
        }
    }
}