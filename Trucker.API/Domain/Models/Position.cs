﻿using System;
using Trucker.API.Domain.Models.ValueObjects.Position;

namespace Trucker.API.Domain.Models
{
    // TODO: Country and code should also be a ValueObject representation, but because I am only accounting for a direct path between point A and B, it would throw an exception.
    public class Position
    {
        public Guid Id { get; init; }
        public Guid TruckPlanId { get; init; }
        public DateTime Timespan { get; init; }
        public string Country { get; init; }
        public string Continent { get; init; }
        public Latitude Latitude { get; init; }
        public Longitude Longitude { get; init; }
    }
}