﻿using System;
using Trucker.API.Domain.Models.ValueObjects.Driver;

namespace Trucker.API.Domain.Models
{
    public class Driver
    {
        public Guid Id { get; init; }
        public Name Name { get; init; }
        public Birthdate Birthdate { get; init; }
        public Nationality Nationality { get; init; }
        public string DriversLicenseNo { get; init; }
        public DateTime CreatedAt { get; init; }
        public DateTime ModifiedAt { get; init; }
    }
}