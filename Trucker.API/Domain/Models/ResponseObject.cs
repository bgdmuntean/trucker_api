﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace Trucker.API.Domain.Models
{
    public class ResponseObject<T>
    {
        public ResponseObject()
        {
            Message = "No Items were found that correspond to your search criteria";
        }

        public ResponseObject(ICollection<T> items)
        {
            Items = items;
            Count = items.Count;
        }

        public int Count { get; set; }
        public ICollection<T> Items { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string Message { get; set; }
    }
}