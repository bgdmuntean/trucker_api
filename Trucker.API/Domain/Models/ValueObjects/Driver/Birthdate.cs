﻿using System;
using Trucker.API.Domain.Exceptions.ValueObjectExceptions;
using ValueOf;

namespace Trucker.API.Domain.Models.ValueObjects.Driver
{
    public class Birthdate : ValueOf<DateTime, Birthdate>
    {
        private const int MinimumAge = 21;

        protected override void Validate()
        {
            var age = DateTime.Now.Year - Value.Year;
            if (age < MinimumAge)
            {
                throw new MinimumDriverAgeException($"Minimum driver's age is less than {MinimumAge}. Actual age: {age}");
            }
        }
    }
}