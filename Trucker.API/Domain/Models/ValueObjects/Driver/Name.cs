﻿using Trucker.API.Domain.Exceptions.ValueObjectExceptions;
using ValueOf;

namespace Trucker.API.Domain.Models.ValueObjects.Driver
{
    public class Name : ValueOf<(string firstName, string lastName), Name>
    {
        protected override void Validate()
        {
            var (firstName, lastName) = Value;

            if (string.IsNullOrEmpty(firstName) && string.IsNullOrEmpty(lastName))
            {
                throw new IllegalNameException($"[FirstName] and [LastName] cannot be null or empty. Current values: FirstName - {firstName}. LastName - {lastName}");
            }

            if (string.IsNullOrWhiteSpace(firstName))
                throw new IllegalNameException($"[FirstName] cannot be null or empty. Current value: {firstName}");

            if (string.IsNullOrWhiteSpace(lastName))
                throw new IllegalNameException($"[LastName] cannot be null or empty. Current value: {lastName}");
        }
    }
}