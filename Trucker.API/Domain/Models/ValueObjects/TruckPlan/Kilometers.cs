﻿using ValueOf;
using Trucker.API.Domain.Exceptions.ValueObjectExceptions;

namespace Trucker.API.Domain.Models.ValueObjects.TruckPlan
{
    public class Kilometers : ValueOf<decimal, Kilometers>
    {
        private const int MinimumValue = 0;

        protected override void Validate()
        {
            if (Value < MinimumValue)
            {
                throw new MinimumDistanceException($"Minimum distance cannot be bellow {MinimumValue}. Actual value: {Value}");
            }
        }
    }
}