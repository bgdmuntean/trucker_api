﻿using Trucker.API.Domain.Exceptions.ValueObjectExceptions;
using ValueOf;

namespace Trucker.API.Domain.Models.ValueObjects.TruckPlan
{
    public class Duration : ValueOf<decimal, Duration>
    {
        private const int MinimumValue = 0;

        protected override void Validate()
        {
            if (Value < MinimumValue)
            {
                throw new MinimumDurationException($"Minimum duration cannot be bellow {MinimumValue}. Actual value: {Value}");
            }
        }
    }
}