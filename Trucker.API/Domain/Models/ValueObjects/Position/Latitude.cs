﻿using Trucker.API.Domain.Exceptions.ValueObjectExceptions;
using ValueOf;

namespace Trucker.API.Domain.Models.ValueObjects.Position
{
    public class Latitude : ValueOf<decimal, Latitude>
    {
        private const decimal MinValue = -90;
        private const decimal MaxValue = 90;

        protected override void Validate()
        {
            if (Value > MaxValue || Value < MinValue)
            {
                throw new IllegalLatitudeException($"The given value is outside of the bounds of latitude ({MinValue} - {MaxValue}). Actual value: {Value}");
            }
        }
    }
}