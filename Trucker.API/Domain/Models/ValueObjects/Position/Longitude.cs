﻿using Trucker.API.Domain.Exceptions.ValueObjectExceptions;
using ValueOf;

namespace Trucker.API.Domain.Models.ValueObjects.Position
{
    public class Longitude : ValueOf<decimal, Longitude>
    {
        private const decimal MinValue = -180;
        private const decimal MaxValue = 180;

        protected override void Validate()
        {
            if (Value > MaxValue || Value < MinValue)
            {
                throw new IllegalLongitudeException($"The given value is outside of the bounds of longitude ({MinValue} - {MaxValue}). Actual value: {Value}");
            }
        }
    }
}