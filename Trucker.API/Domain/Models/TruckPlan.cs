﻿using System;
using Trucker.API.Domain.Models.ValueObjects.TruckPlan;

namespace Trucker.API.Domain.Models
{
    public class TruckPlan
    {
        public Guid Id { get; init; }
        public Guid DriverId { get; init; }
        public string Start { get; init; }
        public string Finish { get; init; }
        public Duration Duration { get; init; }
        public Kilometers Kilometers { get; init; }
    }
}