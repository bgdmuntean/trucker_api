﻿using System.Collections.Generic;

namespace Trucker.API.Domain.Geocoding
{
    public class GeocodingResponse
    {
        public ICollection<GeocodingResult> Results{ get; set; }

    }

    public class GeocodingResult
    {
        public float Confidence { get; set; }
        public string Formatted { get; set; }
        public GeocodingGeometry Geometry { get; set; }
        public GeocodingComponents Components { get; set; }

    }

    public class GeocodingGeometry
    {
        public decimal Lat { get; set; }
        public decimal Lng { get; set; }
    }

    public class GeocodingComponents
    {
        public string Country { get; set; } = "SEA";
        public string Continent { get; set; } = "SEA";
        public string City { get; set; } = "SEA";
    }
}
