﻿using Microsoft.Extensions.Configuration;

namespace Trucker.API.Core.Configuration
{
    internal class ConfigurationManager
    {
        public static IConfiguration AppSettings { get; set; }
    }
}