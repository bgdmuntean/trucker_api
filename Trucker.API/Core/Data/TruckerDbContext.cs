﻿using Microsoft.EntityFrameworkCore;
using Trucker.API.Domain.DTOs;


namespace Trucker.API.Core.Data
{
    public class TruckerDbContext : DbContext
    {
        public TruckerDbContext(DbContextOptions<TruckerDbContext> options) : base(options)
        {
        }

        public virtual DbSet<DriverDto> Drivers { get; set; }
        public virtual DbSet<PositionDto> Positions { get; set; }
        public virtual DbSet<TruckPlanDto> TruckPlans { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasAnnotation("Relational:Collation", "SQL_Latin1_General_CP1_CI_AS");

            modelBuilder.Entity<DriverDto>(entity =>
            {
                entity.ToTable("Driver");

                entity.Property(e => e.Id).HasDefaultValueSql("(newid())");

                entity.Property(e => e.DriversLicenseNo)
                    .IsRequired()
                    .HasMaxLength(128);

                entity.Property(e => e.FirstName)
                    .IsRequired()
                    .HasMaxLength(128);

                entity.Property(e => e.LastName)
                    .IsRequired()
                    .HasMaxLength(128);

                entity.Property(e => e.Nationality)
                    .IsRequired()
                    .HasMaxLength(128);
            });

            modelBuilder.Entity<PositionDto>(entity =>
            {
                entity.ToTable("Position");

                entity.Property(e => e.Id).HasDefaultValueSql("(newid())");

                entity.Property(e => e.Continent)
                    .IsRequired()
                    .HasMaxLength(128);

                entity.Property(e => e.Country)
                    .IsRequired()
                    .HasMaxLength(128);

                entity.Property(e => e.Latitude).HasColumnType("decimal(8, 6)");

                entity.Property(e => e.Longitude).HasColumnType("decimal(9, 6)");

                entity.HasOne(d => d.TruckPlan)
                    .WithMany(p => p.Positions)
                    .HasForeignKey(d => d.TruckPlanId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__Position__TruckP__3E52440B");
            });

            modelBuilder.Entity<TruckPlanDto>(entity =>
            {
                entity.ToTable("TruckPlan");

                entity.Property(e => e.Id).HasDefaultValueSql("(newid())");

                entity.Property(e => e.Duration).HasColumnType("decimal(5, 2)");

                entity.Property(e => e.Finish)
                    .IsRequired()
                    .HasMaxLength(128);

                entity.Property(e => e.Kilometers).HasColumnType("decimal(5, 2)");

                entity.Property(e => e.Start)
                    .IsRequired()
                    .HasMaxLength(128);

                entity.HasOne(d => d.Driver)
                    .WithMany(p => p.TruckPlans)
                    .HasForeignKey(d => d.DriverId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__TruckPlan__Drive__3A81B327");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        private static void OnModelCreatingPartial(ModelBuilder modelBuilder)
        {
        }
    }
}
