using System;
using System.IO;
using System.Reflection;
using AutoMapper;
using FluentValidation.AspNetCore;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.OpenApi.Models;
using Trucker.API.Core.Configuration;
using Trucker.API.Core.Data;
using Trucker.API.Core.Filters;
using Trucker.API.Core.Middleware;
using Trucker.API.Repositories.Drivers;
using Trucker.API.Repositories.Geocoding;
using Trucker.API.Repositories.Positions;
using Trucker.API.Repositories.TruckPlans;
using Trucker.API.Services.Drivers;
using Trucker.API.Services.Filters;
using Trucker.API.Services.TruckLocations;
using Trucker.API.Services.TruckPlans;

namespace Trucker.API
{
    public class Startup
    {
        public IConfiguration Configuration { get; }

        public Startup(IHostEnvironment env)
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile("appsettings.json", false, true)
                .AddEnvironmentVariables();

            Configuration = builder.Build();
            ConfigurationManager.AppSettings = Configuration;
        }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers().AddNewtonsoftJson();

            ConfigureSqlDatabase(services);
            ConfigureDependencyInjection(services);
            ConfigureFluentValidation(services);
            ConfigureSwagger(services);
        }


        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
           
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseHsts();
                app.UseDeveloperExceptionPage();
            }

            app.UseNativeGlobalExceptionHandler();
            app.UseRouting();
            app.UseSwagger();

            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "Trucker API");
                c.EnableFilter();
                c.DisplayOperationId();
                c.DisplayRequestDuration();
            });

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }


        private void ConfigureSqlDatabase(IServiceCollection services)
        {
            services.AddDbContextPool<TruckerDbContext>(options => options.UseSqlServer(Configuration["ConnectionStrings:TruckerDatabaseConnectionString"]));
        }

        private static void ConfigureDependencyInjection(IServiceCollection services)
        {
            services.AddScoped<IDriverRepository, DriverRepository>();
            services.AddScoped<IPositionRepository, PositionRepository>();
            services.AddScoped<ITruckPlanRepository, TruckPlanRepository>();
            services.AddScoped<IGeocodingRepository, GeocodingRepository>();

            services.AddTransient<IDriverService, DriverService>();
            services.AddTransient<ITruckLocationService, TruckLocationService>();
            services.AddTransient<ITruckPlanService, TruckPlanService>();
            services.AddTransient<IFilterService, FilterService>();
        }

        private static void ConfigureFluentValidation(IServiceCollection services)
        {
            services.AddControllers(options =>
                {
                    options.Filters.Add(new ValidationFilter());
                })
                .AddFluentValidation(options =>
                {
                    options.RegisterValidatorsFromAssemblyContaining<Startup>();
                });
        }

        private static void ConfigureSwagger(IServiceCollection services)
        {
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo
                {
                    Title = "Trucker API",
                    Version = "v1",
                    Contact = new OpenApiContact
                    {
                        Name = "Bogdan Muntean",
                        Email = "bgdmuntean@hotmail.com"
                    }
                });

                var xmlFile = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
                var xmlPath = Path.Combine(AppContext.BaseDirectory, xmlFile);
                c.IncludeXmlComments(xmlPath);
                c.EnableAnnotations();
            });
        }
    }
}
