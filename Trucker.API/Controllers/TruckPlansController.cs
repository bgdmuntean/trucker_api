﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Trucker.API.Domain.Contracts.Requests;
using Trucker.API.Domain.Contracts.Responses;
using Trucker.API.Domain.Exceptions;
using Trucker.API.Domain.Models;
using Trucker.API.Services.TruckPlans;

namespace Trucker.API.Controllers
{
    [Produces("application/json")]
    [Route("api/[controller]")]
    [ApiController]
    public class TruckPlansController : ControllerBase
    {
        private readonly ITruckPlanService _truckPlanService;

        public TruckPlansController(ITruckPlanService truckPlanService)
        {
            _truckPlanService = truckPlanService;
        }

        /// <summary>
        /// Create Truck Plan - OPTIONAL
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     POST api/truck-plan
        ///
        /// </remarks>
        /// <response code="200">Returns a response object with a truck plan response object - indicating success</response>
        /// <response code="400">If validation criteria are not met</response>
        /// <response code="500">In case of errors occurring on the data layer or anywhere on the server's side</response>
        [HttpPost("/api/truck-plan")]
        [ProducesResponseType(typeof(ResponseObject<TruckPlanResponse>), StatusCodes.Status201Created)]
        [ProducesResponseType(typeof(ErrorResponse), StatusCodes.Status400BadRequest)]
        [ProducesResponseType(typeof(ErrorResponse), StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<ResponseObject<TruckPlanResponse>>> CreateTruckPlan([FromBody, BindRequired] CreateTruckPlanRequest request)
        {
            return Created("", await _truckPlanService.CreateTruckPlanAsync(request).ConfigureAwait(false));
        }


        /// <summary>
        /// Get Truck Plans - OPTIONAL
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     GET api/truck-plan
        ///
        /// </remarks>
        /// <response code="200">Returns a response object with a truck plan response object - indicating success</response>
        /// <response code="400">If validation criteria are not met</response>
        /// <response code="500">In case of errors occurring on the data layer or anywhere on the server's side</response>
        [HttpGet("/api/truck-plan")]
        [ProducesResponseType(typeof(ResponseObject<TruckPlanResponse>), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(ErrorResponse), StatusCodes.Status400BadRequest)]
        [ProducesResponseType(typeof(ErrorResponse), StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<ResponseObject<TruckPlanResponse>>> GetTruckPlans()
        {

            return Ok(await _truckPlanService.GetTruckPlansAsync().ConfigureAwait(false));
        }
    }
}