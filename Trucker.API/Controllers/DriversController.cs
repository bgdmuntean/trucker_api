﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Trucker.API.Domain.Contracts.Requests;
using Trucker.API.Domain.Contracts.Responses;
using Trucker.API.Domain.Exceptions;
using Trucker.API.Domain.Models;
using Trucker.API.Repositories.Geocoding;
using Trucker.API.Services.Drivers;

namespace Trucker.API.Controllers
{
    [Produces("application/json")]
    [Route("api/[controller]")]
    [ApiController]
    public class DriversController : ControllerBase
    {

        private readonly IDriverService _driverService;

        public DriversController(IDriverService driverService)
        {
            _driverService = driverService;
        }

        /// <summary>
        /// Create driver - OPTIONAL, helpful to generate data.
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     POST api/drivers
        ///
        /// </remarks>
        /// <response code="201">Returns a response object with the newly created driver - indicating success</response>
        /// <response code="400">If validation criteria are not met</response>
        /// <response code="409">If a driver with the same drivers license number exists</response>
        /// <response code="500">In case of errors occurring on the data layer or anywhere on the server's side</response>
        [HttpPost]
        [ProducesResponseType(typeof(ResponseObject<DriverResponse>), StatusCodes.Status201Created)]
        [ProducesResponseType(typeof(ErrorResponse), StatusCodes.Status400BadRequest)]
        [ProducesResponseType(typeof(ErrorResponse), StatusCodes.Status409Conflict)]
        [ProducesResponseType(typeof(ErrorResponse), StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<ResponseObject<DriverResponse>>> CreateDriver([FromBody, BindRequired] CreateDriverRequest request)
        {

            return Created("", await _driverService.CreateDriverAsync(request).ConfigureAwait(false));
        }

        /// <summary>
        /// Get drivers - OPTIONAL, helpful to generate data.
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     GET api/drivers
        ///
        /// </remarks>
        /// <response code="200">Returns a response object with the drivers - indicating success</response>
        /// <response code="400">If validation criteria are not met</response>
        /// <response code="500">In case of errors occurring on the data layer or anywhere on the server's side</response>
        [HttpGet]
        [ProducesResponseType(typeof(ResponseObject<DriverResponse>), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(ErrorResponse), StatusCodes.Status400BadRequest)]
        [ProducesResponseType(typeof(ErrorResponse), StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<ResponseObject<DriverResponse>>> GetDrivers()
        {

            return Ok(await _driverService.GetAllDrivers().ConfigureAwait(false));
        }
    }
}