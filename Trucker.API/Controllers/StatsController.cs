﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Trucker.API.Domain.Contracts.Requests;
using Trucker.API.Domain.Contracts.Responses;
using Trucker.API.Domain.Exceptions;
using Trucker.API.Domain.Models;
using Trucker.API.Services.Filters;

namespace Trucker.API.Controllers
{
    [Produces("application/json")]
    [Route("api/[controller]")]
    [ApiController]
    public class StatsController : ControllerBase
    {
        private readonly IFilterService _filterService;

        public StatsController(IFilterService filterService)
        {
            _filterService = filterService;
        }

        /// <summary>
        /// Get stats - drivers over age that drove withing a country in a timespan
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     GET api/stats
        ///
        /// </remarks>
        /// <response code="200">Returns a response object with the drivers - indicating success</response>
        /// <response code="400">If validation criteria are not met</response>
        /// <response code="404">If no match is found</response>
        /// <response code="500">In case of errors occurring on the data layer or anywhere on the server's side</response>
        [HttpGet]
        [ProducesResponseType(typeof(ResponseObject<FilterResponse>), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(ErrorResponse), StatusCodes.Status400BadRequest)]
        [ProducesResponseType(typeof(ErrorResponse), StatusCodes.Status404NotFound)]
        [ProducesResponseType(typeof(ErrorResponse), StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<ResponseObject<FilterResponse>>> GetDrivers([FromQuery, BindRequired] FilterRequest request)
        {
            var stats = await _filterService.GetCountryKilometersAsync(request).ConfigureAwait(false);
            return Ok(stats);
        }
    }
}