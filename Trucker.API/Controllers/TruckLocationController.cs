﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Trucker.API.Domain.Contracts.Requests;
using Trucker.API.Domain.Contracts.Responses;
using Trucker.API.Domain.Exceptions;
using Trucker.API.Domain.Models;
using Trucker.API.Services.TruckLocations;

namespace Trucker.API.Controllers
{
    [Produces("application/json")]
    [Route("api/[controller]")]
    [ApiController]
    public class TruckLocationController
    {
        private readonly ITruckLocationService _truckLocationService;

        public TruckLocationController(ITruckLocationService truckLocationService)
        {
            _truckLocationService = truckLocationService;
        }

        /// <summary>
        /// Get Truck Location based on coordinates
        /// Latitude between -90 and 90
        /// Longitude between -180 and 180
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     GET api/truck-location
        ///
        /// </remarks>
        /// <response code="200">Returns a response object with a truck location response object - indicating success</response>
        /// <response code="400">If validation criteria are not met</response>
        /// <response code="500">In case of errors occurring on the data layer or anywhere on the server's side</response>
        [HttpGet("/api/truck-location")]
        [ProducesResponseType(typeof(ResponseObject<TruckLocationResponse>), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(ErrorResponse), StatusCodes.Status400BadRequest)]
        [ProducesResponseType(typeof(ErrorResponse), StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<ResponseObject<TruckLocationResponse>>> GetTruckLocation([FromQuery, BindRequired] TruckLocationRequest request)
        {

            return new OkObjectResult(await _truckLocationService.GetTruckLocationAsync(request).ConfigureAwait(false));
        }
    }
}