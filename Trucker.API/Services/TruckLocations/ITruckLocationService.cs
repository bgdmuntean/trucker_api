﻿using System.Threading.Tasks;
using Trucker.API.Domain.Contracts.Requests;
using Trucker.API.Domain.Contracts.Responses;
using Trucker.API.Domain.Models;

namespace Trucker.API.Services.TruckLocations
{
    public interface ITruckLocationService
    {
        Task<ResponseObject<TruckLocationResponse>> GetTruckLocationAsync(TruckLocationRequest request);
    }
}