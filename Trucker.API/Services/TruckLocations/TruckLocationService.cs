﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Trucker.API.Domain.Contracts.Requests;
using Trucker.API.Domain.Contracts.Responses;
using Trucker.API.Domain.Models;
using Trucker.API.Repositories.Geocoding;

namespace Trucker.API.Services.TruckLocations
{
    public class TruckLocationService : ITruckLocationService
    {
        private readonly IGeocodingRepository _geocodingRepository;

        public TruckLocationService(IGeocodingRepository geocodingRepository)
        {
            _geocodingRepository = geocodingRepository;
        }

        public async Task<ResponseObject<TruckLocationResponse>> GetTruckLocationAsync(TruckLocationRequest request)
        {
            var location = await _geocodingRepository
                .GetLocationBasedOnCoordinatesAsync(request.Longitude, request.Latitude).ConfigureAwait(false);

            if (!location.Results.Any())
            {
                return new ResponseObject<TruckLocationResponse>();
            }

            return new ResponseObject<TruckLocationResponse>(
                new List<TruckLocationResponse>
                {
                    new()
                    {
                        Country = location.Results.FirstOrDefault()?.Components.Country,
                        Longitude = location.Results.FirstOrDefault().Geometry.Lng,
                        Latitude = location.Results.FirstOrDefault().Geometry.Lat
                    }
                });
        }
    }
}