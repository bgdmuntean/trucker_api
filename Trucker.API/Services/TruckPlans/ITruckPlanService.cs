﻿using System.Threading.Tasks;
using Trucker.API.Domain.Contracts.Requests;
using Trucker.API.Domain.Contracts.Responses;
using Trucker.API.Domain.Models;

namespace Trucker.API.Services.TruckPlans
{
    public interface ITruckPlanService
    {
        Task<ResponseObject<TruckPlanResponse>> CreateTruckPlanAsync(CreateTruckPlanRequest request);
        Task<ResponseObject<TruckPlanResponse>> GetTruckPlansAsync();
    }
}