﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Trucker.API.Domain.Contracts.Requests;
using Trucker.API.Domain.Contracts.Responses;
using Trucker.API.Domain.Models;
using Trucker.API.Repositories.Geocoding;
using Trucker.API.Repositories.Positions;
using Trucker.API.Repositories.TruckPlans;
using Trucker.API.Utils.Extensions;
using GeoCoordinatePortable;
using Trucker.API.Domain.Exceptions.ServiceExceptions;
using Trucker.API.Domain.Geocoding;
using Trucker.API.Repositories.Drivers;

namespace Trucker.API.Services.TruckPlans
{
    public class TruckPlanService : ITruckPlanService
    {
        private readonly ITruckPlanRepository _truckPlanRepository;
        private readonly IPositionRepository _positionRepository;
        private readonly IGeocodingRepository _geocodingRepository;
        private readonly IDriverRepository _driverRepository;

        public TruckPlanService(ITruckPlanRepository truckPlanRepository, IPositionRepository positionRepository, IGeocodingRepository geocodingRepository, IDriverRepository driverRepository)
        {
            _truckPlanRepository = truckPlanRepository;
            _positionRepository = positionRepository;
            _geocodingRepository = geocodingRepository;
            _driverRepository = driverRepository;
        }


        public async Task<ResponseObject<TruckPlanResponse>> CreateTruckPlanAsync(CreateTruckPlanRequest request)
        {
            var driver = await _driverRepository.GetDriversAsync();

            if (driver.All(_ => _.Id != request.DriverId))
            {
                throw new DriverNotFoundException($"No driver with Id {request.DriverId} exists.");
            }

            var startPoint = await GetGeocodingResponse(request.StartLongitude, request.StartLatitude);
            var finishPoint = await GetGeocodingResponse(request.FinishLongitude, request.FinishLatitude);

            var distanceMeters = CalculateDistanceMeters(request.Positions);
            var km = distanceMeters / 1000;

            var duration = request
                .Positions
                .Max(_ => _.Timespan)
                .Subtract(request.Positions.Min(_ => _.Timespan))
                .Minutes; 

            var truckPlan = await _truckPlanRepository
                .CreateTruckPlanAsync(
                    request.DriverId,
                    startPoint.Results.FirstOrDefault()?.Components.Country,
                    finishPoint.Results.FirstOrDefault()?.Components.Country,
                    Convert.ToDecimal(km),
                    Convert.ToDecimal(duration))
                .ConfigureAwait(false);
            var truckPlanResponse = truckPlan.ToResponseObject();

            await CreatePositionsForTruckPlan(request, truckPlan, truckPlanResponse);

            return new ResponseObject<TruckPlanResponse>(new List<TruckPlanResponse> {truckPlanResponse});
        }

        private async Task CreatePositionsForTruckPlan(CreateTruckPlanRequest request, TruckPlan truckPlan,
            TruckPlanResponse truckPlanResponse)
        {
            foreach (var positionRequest in request.Positions.OrderBy(_ => _.Timespan))
            {
                var location = await GetGeocodingResponse(positionRequest.Longitude, positionRequest.Latitude);
                var position = await _positionRepository.CreatePositionAsync(
                        truckPlan.Id,
                        positionRequest.Timespan,
                        positionRequest.Longitude,
                        positionRequest.Latitude,
                        location.Results.FirstOrDefault()?.Components.Continent,
                        location.Results.FirstOrDefault()?.Components.Country)
                    .ConfigureAwait(false);
                truckPlanResponse.Positions.Add(position.ToResponseObject());
            }
        }

        public async Task<ResponseObject<TruckPlanResponse>> GetTruckPlansAsync()
        {

            var truckPlans = await _truckPlanRepository
                .GetTruckPlansAsync()
                .ConfigureAwait(false);

            var responseObjects = truckPlans.Select(_ => _.ToResponseObject()).ToList();

            foreach (var truckPlan in responseObjects)
            {
                truckPlan.Positions = (await _positionRepository
                    .GetPositionByTruckPlanAsync(truckPlan.Id)
                    .ConfigureAwait(false))
                    .Select(_ => _.ToResponseObject())
                    .ToList();
            }

            return new ResponseObject<TruckPlanResponse>(responseObjects);
        }

        private static double CalculateDistanceMeters(ICollection<CreatePositionRequest> requestPositions)
        {
            var distance = 0.0;

            var orderedPositions = requestPositions.OrderBy(_ => _.Timespan).ToArray();
            for (var i = 0; i < orderedPositions.Length - 1; i++)
            {
                var start = new GeoCoordinate(Convert.ToDouble(orderedPositions[i].Latitude), Convert.ToDouble(orderedPositions[i].Longitude));
                var finish = new GeoCoordinate(Convert.ToDouble(orderedPositions[i + 1].Latitude), Convert.ToDouble(orderedPositions[i + 1].Longitude));

                distance += start.GetDistanceTo(finish);
            }
            return distance;
        }

        private async Task<GeocodingResponse> GetGeocodingResponse(decimal longitude, decimal latitude)
        {
            var point = await _geocodingRepository
                .GetLocationBasedOnCoordinatesAsync(longitude, latitude)
                .ConfigureAwait(false);
            return point;
        }

      
    }
}