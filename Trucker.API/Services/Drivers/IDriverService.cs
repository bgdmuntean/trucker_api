﻿using System.Threading.Tasks;
using Trucker.API.Domain.Contracts.Requests;
using Trucker.API.Domain.Contracts.Responses;
using Trucker.API.Domain.Models;

namespace Trucker.API.Services.Drivers
{
    public interface IDriverService
    {
        Task<ResponseObject<DriverResponse>> CreateDriverAsync(CreateDriverRequest request);
        Task<ResponseObject<DriverResponse>> GetAllDrivers();
    }
}