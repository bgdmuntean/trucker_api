﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Trucker.API.Domain.Contracts.Requests;
using Trucker.API.Domain.Contracts.Responses;
using Trucker.API.Domain.Exceptions;
using Trucker.API.Domain.Models;
using Trucker.API.Repositories.Drivers;
using Trucker.API.Utils.Extensions;

namespace Trucker.API.Services.Drivers
{
    public class DriverService : IDriverService
    {
        private readonly IDriverRepository _driverRepository;

        public DriverService(IDriverRepository driverRepository)
        {
            _driverRepository = driverRepository;
        }


        public async Task<ResponseObject<DriverResponse>> CreateDriverAsync(CreateDriverRequest request)
        {
            var drivers = await _driverRepository
                .GetDriversAsync()
                .ConfigureAwait(false);

            if (drivers.FirstOrDefault(_ => string.Equals(_.DriversLicenseNo, request.DriversLicenseNo, StringComparison.InvariantCultureIgnoreCase)) != null)
            {
                throw new DriverCreationException(HttpStatusCode.Conflict,
                    $"A driver with the same drivers license number already exists. ({request.DriversLicenseNo})");
            }

            var driver = await _driverRepository.CreateDriverAsync(request.FirstName, request.LastName,
                    request.Birthdate, request.Nationality, request.DriversLicenseNo)
                .ConfigureAwait(false);

            return new ResponseObject<DriverResponse>(new List<DriverResponse> {driver.ToResponseObject()});
        }

        public async Task<ResponseObject<DriverResponse>> GetAllDrivers()
        {
            var drivers = await _driverRepository
                .GetDriversAsync()
                .ConfigureAwait(false);

            return new ResponseObject<DriverResponse>(drivers.Select(_ => _.ToResponseObject()).ToList());
        }
    }
}