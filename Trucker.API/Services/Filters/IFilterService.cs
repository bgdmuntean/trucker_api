﻿using System.Threading.Tasks;
using Trucker.API.Domain.Contracts.Requests;
using Trucker.API.Domain.Contracts.Responses;
using Trucker.API.Domain.Models;

namespace Trucker.API.Services.Filters
{
    public interface IFilterService
    {
        Task<ResponseObject<FilterResponse>> GetCountryKilometersAsync(FilterRequest request);
    }
}