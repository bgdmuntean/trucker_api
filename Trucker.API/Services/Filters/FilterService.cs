﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GeoCoordinatePortable;
using Trucker.API.Domain.Contracts.Requests;
using Trucker.API.Domain.Contracts.Responses;
using Trucker.API.Domain.Exceptions.ServiceExceptions;
using Trucker.API.Domain.Models;
using Trucker.API.Repositories.Drivers;
using Trucker.API.Repositories.Positions;
using Trucker.API.Repositories.TruckPlans;

namespace Trucker.API.Services.Filters
{
    public class FilterService : IFilterService
    {
        private readonly IDriverRepository _driverRepository;
        private readonly ITruckPlanRepository _truckPlanRepository;
        private readonly IPositionRepository _positionRepository;

        public FilterService(IDriverRepository driverRepository, ITruckPlanRepository truckPlanRepository, IPositionRepository positionRepository)
        {
            _driverRepository = driverRepository;
            _truckPlanRepository = truckPlanRepository;
            _positionRepository = positionRepository;
        }

        public async Task<ResponseObject<FilterResponse>> GetCountryKilometersAsync(FilterRequest request)
        {
            var drivers = await _driverRepository
                .GetDriversOverGivenAgeAsync(request.DriveAgeOver)
                .ConfigureAwait(false);

            if (!drivers.Any())
            {
                throw new NoDriversOverAgeException($"No drivers over the age of {request.DriveAgeOver} were found.");
            }

            var truckPlans = await _truckPlanRepository
                .GetTruckPlansOfDriversAsync(drivers.Select(_ => _.Id)
                    .ToArray())
                .ConfigureAwait(false);

            if (!truckPlans.Any())
            {
                throw new NoTruckPlansForDriversException("No truck plans were found belonging to the given drivers");
            }

            var positions = await _positionRepository
                .GetPositionByTruckPlansAndCountryAsync(truckPlans.Select(_ => _.Id)
                    .ToArray(), request.Country)
                .ConfigureAwait(false);

            if (!positions.Any())
            {
                throw new NoPositionsBelongingToTruckPlansFromCountryException($"There are no positions from country {request.Country} belonging to the given truck plans.");
            }

            positions = positions.Where(_ => _.Timespan >= request.DateFrom && _.Timespan <= request.DateTo).ToList();

            if (!positions.Any())
            {
                throw new NoPositionsInTimeIntervalAndCountryException($"There are no positions from country {request.Country} between dates {request.DateFrom} and {request.DateTo}.");
            }

            var truckPlanPositionGroups = positions.GroupBy(_ => _.TruckPlanId);

            var kilometers = truckPlanPositionGroups
                .Where(truckPlanPositions => truckPlanPositions.Count() != 1)
                .Sum(GetKilometersFromTruckPlanPositions);

            return new ResponseObject<FilterResponse>(new List<FilterResponse>(){new FilterResponse
            {
                Kilometers = kilometers,
                AgeOver = request.DriveAgeOver,
                DateFrom = request.DateFrom,
                DateTo = request.DateTo,
                Country = request.Country
            }});
        }

        private static decimal GetKilometersFromTruckPlanPositions(IGrouping<Guid, Position> truckPlanPositions)
        {
            var distance = 0.0;

            var orderedPositions = truckPlanPositions.OrderBy(_ => _.Timespan).ToArray();
            for (var i = 0; i < orderedPositions.Length - 1; i++)
            {
                var start = new GeoCoordinate(Convert.ToDouble(orderedPositions[i].Latitude), Convert.ToDouble(orderedPositions[i].Longitude));
                var finish = new GeoCoordinate(Convert.ToDouble(orderedPositions[i + 1].Latitude), Convert.ToDouble(orderedPositions[i + 1].Longitude));

                distance += start.GetDistanceTo(finish);
            }
            return Convert.ToDecimal(distance / 1000);
        }
    }
}