﻿using Trucker.API.Domain.DTOs;
using Trucker.API.Domain.Models;
using Trucker.API.Domain.Models.ValueObjects.Driver;
using Trucker.API.Domain.Models.ValueObjects.Position;
using Trucker.API.Domain.Models.ValueObjects.TruckPlan;

namespace Trucker.API.Utils.Extensions
{
    public static class DomainEntityExtensions
    {
        public static Driver ToModel(this DriverDto driver)
            => new()
            {
                Id = driver.Id,
                CreatedAt = driver.CreatedAt.DateTime,
                ModifiedAt = driver.ModifiedAt.DateTime,
                DriversLicenseNo = driver.DriversLicenseNo,
                Name = Name.From((driver.FirstName, driver.LastName)),
                Nationality = Nationality.From(driver.Nationality),
                Birthdate = Birthdate.From(driver.Birthdate.DateTime)
            };

        public static TruckPlan ToModel(this TruckPlanDto truckPlan)
            => new()
            {
                Id = truckPlan.Id,
                DriverId = truckPlan.DriverId,
                Start = truckPlan.Start,
                Finish = truckPlan.Finish,
                Kilometers = Kilometers.From(truckPlan.Kilometers),
                Duration = Duration.From(truckPlan.Duration)
            };

        public static Position ToModel(this PositionDto position)
            => new()
            {
                Id = position.Id,
                TruckPlanId = position.TruckPlanId,
                Timespan = position.Timestamp.DateTime,
                Latitude = Latitude.From(position.Latitude),
                Longitude = Longitude.From(position.Longitude),
                Continent = position.Continent,
                Country = position.Country
            };
    }
}
