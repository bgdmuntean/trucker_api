﻿using Trucker.API.Domain.Contracts.Responses;
using Trucker.API.Domain.Models;

namespace Trucker.API.Utils.Extensions
{
    public static class ResponseObjectExtensions
    {

        public static DriverResponse ToResponseObject(this Driver driver)
            => new()
            {
                Id = driver.Id,
                FirstName = driver.Name.Value.firstName,
                LastName = driver.Name.Value.lastName,
                Nationality = driver.Nationality.Value,
                Birthdate = driver.Birthdate.Value,
                DriversLicenseNo = driver.DriversLicenseNo
            };

        public static TruckPlanResponse ToResponseObject(this TruckPlan truckPlan)
            => new()
            {
                Id = truckPlan.Id,
                DriverId = truckPlan.DriverId,
                Start = truckPlan.Start,
                Duration = truckPlan.Duration.Value,
                Kilometers = truckPlan.Kilometers.Value,
                Finish = truckPlan.Finish
            };

        public static PositionResponse ToResponseObject(this Position position)
            => new()
            {
                Id = position.Id,
                TruckPlanId = position.TruckPlanId,
                Longitude = position.Longitude,
                Latitude = position.Latitude,
                Continent = position.Continent,
                Country = position.Country,
                Timestamp = position.Timespan
            };
    }
}