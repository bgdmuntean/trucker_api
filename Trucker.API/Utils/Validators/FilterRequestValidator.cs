﻿using FluentValidation;
using Trucker.API.Domain.Contracts.Requests;

namespace Trucker.API.Utils.Validators
{
    public class FilterRequestValidator : AbstractValidator<FilterRequest>
    {
        public FilterRequestValidator()
        {
            RuleFor(_ => _.DriveAgeOver)
                .NotEmpty()
                .WithMessage("[DriveAgeOver] cannot be empty or null")
                .GreaterThanOrEqualTo(21);

            RuleFor(_ => _.Country)
                .NotEmpty()
                .WithMessage("[Country] cannot be empty or null");

            RuleFor(_ => _.DateFrom)
                .NotEmpty()
                .WithMessage("[DateFrom] cannot be empty or null")
                .LessThanOrEqualTo(_ => _.DateTo)
                .WithMessage("[DateTo] cannot be more than [DateFrom]");

            RuleFor(_ => _.DateTo)
                .NotEmpty()
                .WithMessage("[DateTo] cannot be empty or null")
                .GreaterThanOrEqualTo(_ => _.DateFrom)
                .WithMessage("[DateFrom] cannot be less than [DateTo]"); 
        }
    }
}