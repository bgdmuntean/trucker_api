﻿using FluentValidation;
using Trucker.API.Domain.Contracts.Requests;

namespace Trucker.API.Utils.Validators.TruckPlans
{
    public class CreateTruckPlanRequestValidator : AbstractValidator<CreateTruckPlanRequest>
    {
        public CreateTruckPlanRequestValidator()
        {
            RuleFor(_ => _.DriverId)
                .NotEmpty()
                .WithMessage("[DriverId] cannot be empty or null");

            RuleFor(_ => _.StartLatitude)
                .NotEmpty()
                .WithMessage("[StartLatitude] cannot be empty or null")
                .ExclusiveBetween(-90M, 90M)
                .WithMessage("[StartLatitude] must be between -90 and 90 degrees");

            RuleFor(_ => _.StartLongitude)
                .NotEmpty()
                .WithMessage("[StartLongitude] cannot be empty or null")
                .ExclusiveBetween(-180M, 180M)
                .WithMessage("[StartLongitude] must be between -180 and 180 degrees");

            RuleFor(_ => _.FinishLatitude)
                .NotEmpty()
                .WithMessage("[FinishLatitude] cannot be empty or null")
                .ExclusiveBetween(-90M, 90M)
                .WithMessage("[FinishLatitude] must be between -90 and 90 degrees");

            RuleFor(_ => _.FinishLongitude)
                .NotEmpty()
                .WithMessage("[FinishLongitude] cannot be empty or null")
                .ExclusiveBetween(-180M, 180M)
                .WithMessage("[FinishLongitude] must be between -180 and 180 degrees");

            RuleFor(_ => _.Positions)
                .NotEmpty()
                .WithMessage("[Positions] cannot be empty or null")
                .Must(_ => _.Count >= 2)
                .WithMessage("[Positions] cannot be less than 2");
        }
    }
}