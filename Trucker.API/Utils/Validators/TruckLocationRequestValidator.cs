﻿using FluentValidation;
using Trucker.API.Domain.Contracts.Requests;

namespace Trucker.API.Utils.Validators
{
    public class TruckLocationRequestValidator : AbstractValidator<TruckLocationRequest>
    {
        public TruckLocationRequestValidator()
        {
            RuleFor(_ => _.Latitude)
                .NotEmpty()
                .WithMessage("[Latitude] cannot be empty or null")
                .ExclusiveBetween(-90M, 90M)
                .WithMessage("[Latitude] must be between -90 and 90 degrees");

            RuleFor(_ => _.Longitude)
                .NotEmpty()
                .WithMessage("[Longitude] cannot be empty or null")
                .ExclusiveBetween(-180M, 180M)
                .WithMessage("[Longitude] must be between -180 and 180 degrees");
        }   
    }
}