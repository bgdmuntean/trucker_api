﻿using System.Threading.Tasks;
using Trucker.API.Domain.Geocoding;

namespace Trucker.API.Repositories.Geocoding
{
    public interface IGeocodingRepository
    {
        Task<GeocodingResponse> GetLocationBasedOnCoordinatesAsync(decimal longitude, decimal latitude);
    }
}