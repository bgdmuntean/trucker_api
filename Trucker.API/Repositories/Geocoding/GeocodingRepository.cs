﻿using System.Threading.Tasks;
using Newtonsoft.Json;
using RestSharp;
using Trucker.API.Core.Configuration;
using Trucker.API.Domain.Geocoding;

namespace Trucker.API.Repositories.Geocoding
{
    public class GeocodingRepository: IGeocodingRepository
    {
        private readonly IRestClient _client;
        private readonly string _key;

        public GeocodingRepository()
        {
            _key = ConfigurationManager.AppSettings["OpenCageData:Key"];
            _client = new RestClient(ConfigurationManager.AppSettings["OpenCageData:Uri"]);
        }

        public async Task<GeocodingResponse> GetLocationBasedOnCoordinatesAsync(decimal longitude, decimal latitude)
        {
            var request = new RestRequest(Method.GET)
                .AddQueryParameter("key", _key)
                .AddQueryParameter("q", $"{latitude}+{longitude}");

            var response = await _client.ExecuteAsync<object>(request);

            if (response.IsSuccessful)
            {
                return JsonConvert.DeserializeObject<GeocodingResponse>(response.Content);
            }

            throw response.ErrorException;
        }
    }
}