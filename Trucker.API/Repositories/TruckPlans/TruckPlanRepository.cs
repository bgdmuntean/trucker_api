﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Trucker.API.Core.Data;
using Trucker.API.Domain.DTOs;
using Trucker.API.Domain.Models;
using Trucker.API.Utils.Extensions;

namespace Trucker.API.Repositories.TruckPlans
{
    public class TruckPlanRepository : ITruckPlanRepository
    {
        private readonly TruckerDbContext _dbContext;

        public TruckPlanRepository(TruckerDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<TruckPlan> CreateTruckPlanAsync(Guid driverId, string start, string finish, decimal kilometers, decimal duration)
        {
            var truckPlan = new TruckPlanDto
            {
                DriverId = driverId,
                Duration = duration,
                Start = start,
                Finish = finish,
                Kilometers = kilometers
            };

            await _dbContext
                .AddAsync(truckPlan)
                .ConfigureAwait(false);

            await _dbContext
                .SaveChangesAsync()
                .ConfigureAwait(false);

            return truckPlan.ToModel();
        }

        public async Task<ICollection<TruckPlan>> GetTruckPlansAsync()
        {
            var truckPlans = await _dbContext
                .TruckPlans
                .AsNoTracking()
                .ToListAsync()
                .ConfigureAwait(false);

            return truckPlans.Select(_ => _.ToModel()).ToList();
        }

        public async Task<ICollection<TruckPlan>> GetTruckPlansOfDriversAsync(Guid[] driverIds)
        {
            var truckPlans = await _dbContext
                .TruckPlans
                .AsNoTracking()
                .Where(_ => driverIds.Contains(_.DriverId))
                .ToListAsync()
                .ConfigureAwait(false);

            return truckPlans.Select(_ => _.ToModel()).ToList();
        }
    }
}