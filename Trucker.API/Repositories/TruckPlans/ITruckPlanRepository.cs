﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Trucker.API.Domain.Models;

namespace Trucker.API.Repositories.TruckPlans
{
    public interface ITruckPlanRepository
    {
        Task<TruckPlan> CreateTruckPlanAsync(Guid driverId, string start, string finish, decimal kilometers, decimal duration);
        Task<ICollection<TruckPlan>> GetTruckPlansAsync();
        Task<ICollection<TruckPlan>> GetTruckPlansOfDriversAsync(Guid[] driverIds);
    }
}