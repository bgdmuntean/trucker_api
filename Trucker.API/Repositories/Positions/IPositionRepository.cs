﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Trucker.API.Domain.Models;

namespace Trucker.API.Repositories.Positions
{
    public interface IPositionRepository
    {
        Task<Position> CreatePositionAsync(Guid truckPlanId, DateTimeOffset timestamp, decimal longitude, decimal latitude, string continent, string country);
        Task<ICollection<Position>> GetPositionsAsync();
        Task<ICollection<Position>> GetPositionByTruckPlanAsync(Guid truckPlanId);
        Task<ICollection<Position>> GetPositionByTruckPlansAndCountryAsync(Guid[] truckPlanIds, string country);
    }
}