﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.EntityFrameworkCore;
using Trucker.API.Core.Data;
using Trucker.API.Domain.DTOs;
using Trucker.API.Domain.Models;
using Trucker.API.Utils.Extensions;

namespace Trucker.API.Repositories.Positions
{
    public class PositionRepository : IPositionRepository
    {
        private readonly TruckerDbContext _dbContext;

        public PositionRepository(TruckerDbContext dbContext)
        {
            _dbContext = dbContext;
        }


        public async Task<Position> CreatePositionAsync(Guid truckPlanId, DateTimeOffset timestamp, decimal longitude, decimal latitude, string continent, string country)
        {
            try
            {
                var position = new PositionDto
                {
                    TruckPlanId = truckPlanId,
                    Country = country,
                    Continent = continent,
                    Latitude = latitude,
                    Longitude = longitude,
                    Timestamp = timestamp,
                };

                await _dbContext
                    .AddAsync(position)
                    .ConfigureAwait(false);

                await _dbContext
                    .SaveChangesAsync()
                    .ConfigureAwait(false);

                return position.ToModel();
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        public async Task<ICollection<Position>> GetPositionsAsync()
        {
            var positions = await _dbContext
                .Positions
                .AsNoTracking()
                .ToListAsync()
                .ConfigureAwait(false);

            return positions.Select(_ => _.ToModel()).ToList();
        }

        public async Task<ICollection<Position>> GetPositionByTruckPlanAsync(Guid truckPlanId)
        {
            var positions = await _dbContext
                .Positions
                .AsNoTracking()
                .Where(_ => _.TruckPlanId == truckPlanId)
                .ToListAsync()
                .ConfigureAwait(false);

            return positions.Select(_ => _.ToModel()).ToList();
        }

        public async Task<ICollection<Position>> GetPositionByTruckPlansAndCountryAsync(Guid[] truckPlanIds, string country)
        {
            var positions = await _dbContext
                .Positions
                .AsNoTracking()
                .Where(_ => string.Equals(_.Country, country) && truckPlanIds.Contains(_.TruckPlanId))
                .ToListAsync()
                .ConfigureAwait(false);

            return positions.Select(_ => _.ToModel()).ToList();
        }
    }
}