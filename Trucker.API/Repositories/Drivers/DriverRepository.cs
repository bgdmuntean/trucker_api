﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.EntityFrameworkCore;
using Trucker.API.Core.Data;
using Trucker.API.Domain.DTOs;
using Trucker.API.Domain.Models;
using Trucker.API.Utils.Extensions;

namespace Trucker.API.Repositories.Drivers
{
    public class DriverRepository : IDriverRepository
    {
        private readonly TruckerDbContext _dbContext;

        public DriverRepository(TruckerDbContext dbContext)
        {

            _dbContext = dbContext;
        }

        public async Task<Driver> CreateDriverAsync(string firstName, string lastName, DateTime birthdate, string nationality, string driversLicenseNo)
        {
            var driver = new DriverDto
            {
                FirstName = firstName,
                LastName = lastName,
                Birthdate = birthdate,
                DriversLicenseNo = driversLicenseNo,
                Nationality = nationality,
                CreatedAt = DateTime.Now,
                ModifiedAt = DateTime.Now
            };

            await _dbContext
                .AddAsync(driver)
                .ConfigureAwait(false);

            await _dbContext
                .SaveChangesAsync()
                .ConfigureAwait(false);

            return driver.ToModel();
        }

        public async Task<ICollection<Driver>> GetDriversAsync()
        {
            var drivers = await _dbContext
                .Drivers
                .AsNoTracking()
                .ToListAsync()
                .ConfigureAwait(false);

            return drivers.Select(_ => _.ToModel()).ToList();
        }

        public async Task<ICollection<Driver>> GetDriversOverGivenAgeAsync(int age)
        {
            var drivers = await _dbContext
                .Drivers
                .AsNoTracking()
                .Where(_ => DateTime.Now.Year - _.Birthdate.Year >= age)
                .ToListAsync()
                .ConfigureAwait(false);

            return drivers.Select(_ => _.ToModel()).ToList();
        }
    }
}