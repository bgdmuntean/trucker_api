﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Trucker.API.Domain.Models;

namespace Trucker.API.Repositories.Drivers
{
    public interface IDriverRepository
    {
        Task<Driver> CreateDriverAsync(string firstName, string lastName, DateTime birthdate, string nationality, string driversLicenseNo);
        Task<ICollection<Driver>> GetDriversAsync();
        Task<ICollection<Driver>> GetDriversOverGivenAgeAsync(int age);
    }
}