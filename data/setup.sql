-- Create Database if not exists
IF NOT EXISTS(SELECT * FROM sys.databases WHERE name = 'TruckerStorage')
  BEGIN
    CREATE DATABASE [TruckerStorage]
  END
GO


-- Use database 
USE [TruckerStorage]
GO


-- Create tables if not exists

-- Driver
IF NOT EXISTS (
	SELECT * 
	FROM sysobjects 
	WHERE NAME = 'Driver' AND xtype = 'U')
		CREATE TABLE [Driver] (
			[Id] UNIQUEIDENTIFIER PRIMARY KEY DEFAULT NEWID(),
			[FirstName] NVARCHAR(128) NOT NULL,
            [LastName] NVARCHAR(128) NOT NULL,
            [Birthdate] DATETIMEOFFSET NOT NULL,
			[DriversLicenseNo] NVARCHAR(128) NOT NULL,
			[Nationality] NVARCHAR(128) NOT NULL,
			[CreatedAt] DATETIMEOFFSET NOT NULL,
            [ModifiedAt] DATETIMEOFFSET NOT NULL
		)


-- TruckPlan
IF NOT EXISTS (
	SELECT * 
	FROM sysobjects 
	WHERE NAME = 'TruckPlan' AND xtype = 'U')
		CREATE TABLE [TruckPlan] (
			[Id] UNIQUEIDENTIFIER PRIMARY KEY DEFAULT NEWID(),
            [DriverId] UNIQUEIDENTIFIER FOREIGN KEY REFERENCES [Driver](Id)  NOT NULL,
			[Start] NVARCHAR(128) NOT NULL,
            [Finish] NVARCHAR(128) NOT NULL,
            [Kilometers] DECIMAL(5,2) NOT NULL,
			[Duration] DECIMAL(5,2) NOT NULL
		)


-- Position
IF NOT EXISTS (
	SELECT * 
	FROM sysobjects 
	WHERE NAME = 'Position' AND xtype = 'U')
		CREATE TABLE [Position] (
			[Id] UNIQUEIDENTIFIER PRIMARY KEY DEFAULT NEWID(),
            [TruckPlanId] UNIQUEIDENTIFIER FOREIGN KEY REFERENCES [TruckPlan](Id)  NOT NULL,
			[Timestamp] DATETIMEOFFSET NOT NULL,
            [Longitude] DECIMAL(9,6) NOT NULL,
            [Latitude] DECIMAL(8,6) NOT NULL,
			[Continent] NVARCHAR(128) NOT NULL,
			[Country] NVARCHAR(128) NOT NULL
		)

GO