﻿using System;
using System.Threading.Tasks;
using Faker;
using Microsoft.EntityFrameworkCore;
using Trucker.API.Core.Data;
using Trucker.API.Domain.DTOs;
using Trucker.API.Repositories.TruckPlans;
using Xunit;

namespace Trucker.API.Integration.Infrastructure.TruckPlan
{
    public class CreateTruckPlanTests
    {
        private readonly ITruckPlanRepository _sut;
        private readonly Guid _id;
        public CreateTruckPlanTests()
        {
            var options = new DbContextOptionsBuilder<TruckerDbContext>()
                .UseInMemoryDatabase(databaseName: "TruckerStorageTruckPlan")
                .Options;

            _id = Guid.NewGuid();
            var firstName = Faker.Name.FirstName();
            var lastName = Faker.Name.LastName();
            var nationality = "German";
            var driversLicenseNo = Lorem.Word();
            var birthdate = Date.Between(DateTime.Now.AddYears(-30), DateTime.Now.AddYears(-21));

            using (var context = new TruckerDbContext(options))
            {
                context.Drivers.Add(new DriverDto
                {
                    Id = _id,
                    FirstName = firstName,
                    LastName = lastName,
                    Nationality = nationality,
                    DriversLicenseNo = driversLicenseNo,
                    Birthdate = birthdate
                });


                context.TruckPlans.Add(new TruckPlanDto
                {
                    Id = Guid.NewGuid(),
                    DriverId = _id,
                    Duration = 21M,
                    Start = "test",
                    Finish = "test2",
                    Kilometers = 22M
                });

                context.SaveChanges();
            }

            _sut = new TruckPlanRepository(new TruckerDbContext(options));
        }

        [Fact]
        public async Task TruckPlanRepository_CanCreateTruckPlan_WhenAllDataIsCorrect()
        {
            // Arrange

            var start = Address.Country();
            var finish = Address.Country();
            var km = 12M;
            var duration = 133M;

            // Act

            var truckPlan= await _sut.CreateTruckPlanAsync(_id, start, finish, km, duration);

            // Assert

            Assert.IsType<Domain.Models.TruckPlan>(truckPlan);
            Assert.NotNull(truckPlan);
            Assert.Equal(start, truckPlan.Start);
        }
    }
}