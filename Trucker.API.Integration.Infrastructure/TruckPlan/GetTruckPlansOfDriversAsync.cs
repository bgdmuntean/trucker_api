﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Faker;
using Microsoft.EntityFrameworkCore;
using Trucker.API.Core.Data;
using Trucker.API.Domain.DTOs;
using Trucker.API.Repositories.TruckPlans;
using Xunit;

namespace Trucker.API.Integration.Infrastructure.TruckPlan
{
    public class GetTruckPlansOfDriversAsync
    {
        [Fact]
        public async Task TruckPlanRepository_CanReturnTruckPlans_WhenTruckPlansBelongToDrivers()
        {
            // Arrange

            var options = new DbContextOptionsBuilder<TruckerDbContext>()
                .UseInMemoryDatabase(databaseName: "TruckerStorageTruckPlanGetAge")
                .Options;

            var id = Guid.NewGuid();
            var firstName = Name.FirstName();
            var lastName = Name.LastName();
            var nationality = "German";
            var driversLicenseNo = Lorem.Word();
            var birthdate = Date.Between(DateTime.Now.AddYears(-30), DateTime.Now.AddYears(-21));

            await using (var context = new TruckerDbContext(options))
            {
                await context.Drivers.AddAsync(new DriverDto
                {
                    Id = id,
                    FirstName = firstName,
                    LastName = lastName,
                    Nationality = nationality,
                    DriversLicenseNo = driversLicenseNo,
                    Birthdate = birthdate
                });


                await context.TruckPlans.AddAsync(new TruckPlanDto
                {
                    Id = Guid.NewGuid(),
                    DriverId = id,
                    Duration = 21M,
                    Start = "test",
                    Finish = "test2",
                    Kilometers = 22M
                });

                await context.SaveChangesAsync();
            }

            var sut = new TruckPlanRepository(new TruckerDbContext(options));

            // Act

            var truckPlans = await sut.GetTruckPlansOfDriversAsync(new []{id});

            // Assert

            Assert.IsAssignableFrom<ICollection<Domain.Models.TruckPlan>>(truckPlans);
            Assert.NotNull(truckPlans);
            Assert.NotEmpty(truckPlans);
        }


        [Fact]
        public async Task TruckPlanRepository_CanReturnEmptyLists_WhenTruckPlansDoNotExist()
        {
            // Arrange

            var options = new DbContextOptionsBuilder<TruckerDbContext>()
                .UseInMemoryDatabase("TruckerStorageTruckPlanGetAge")
                .Options;

            var sut = new TruckPlanRepository(new TruckerDbContext(options));

            // Act

            var truckPlans = await sut.GetTruckPlansOfDriversAsync(new []{Guid.NewGuid()});

            // Assert

            Assert.IsAssignableFrom<ICollection<Domain.Models.TruckPlan>>(truckPlans);
            Assert.NotNull(truckPlans);
            Assert.Empty(truckPlans);
        }
    }
}