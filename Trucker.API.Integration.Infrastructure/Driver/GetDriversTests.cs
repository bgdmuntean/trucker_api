﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Faker;
using Microsoft.EntityFrameworkCore;
using Trucker.API.Core.Data;
using Trucker.API.Domain.DTOs;
using Trucker.API.Repositories.Drivers;
using Xunit;

namespace Trucker.API.Integration.Infrastructure.Driver
{
    public class GetDriversTests
    {

        [Fact]
        public async Task DriverRepository_CanReturnDrivers_WhenThereAreDrivers()
        {
            // Arrange 

            var options = new DbContextOptionsBuilder<TruckerDbContext>()
                    .UseInMemoryDatabase(databaseName: "TruckerStorage")
                    .Options;

            await using (var context = new TruckerDbContext(options))
            {
                await context.Drivers.AddAsync(new DriverDto
                {
                    Id = Guid.NewGuid(),
                    FirstName = Name.FirstName(),
                    LastName = Name.LastName(),
                    Nationality = "German",
                    DriversLicenseNo = Lorem.Word(),
                    Birthdate = Date.Between(DateTime.Now.AddYears(-30), DateTime.Now.AddYears(-21))
                });

                await context.SaveChangesAsync();
            }

           
            await using (var context = new TruckerDbContext(options))
            {
                // Act

                var sut = new DriverRepository(context);

                var drivers = await sut.GetDriversAsync();

                // Assert

                Assert.IsAssignableFrom<ICollection<Domain.Models.Driver>>(drivers);
                Assert.NotNull(drivers);
                Assert.NotEmpty(drivers);
            }
        }

        [Fact]
        public static async Task DriverRepository_CanReturnEmptyList_WhenThereAreNoDrivers()
        {
            // Arrange 
            var options = new DbContextOptionsBuilder<TruckerDbContext>()
                .UseInMemoryDatabase(databaseName: "TruckerStorage2")
                .Options;


            await using var context = new TruckerDbContext(options);
            var sut = new DriverRepository(context);
            // Act

            var drivers = await sut.GetDriversAsync();

            // Assert

            Assert.IsAssignableFrom<ICollection<Domain.Models.Driver>>(drivers);
            Assert.NotNull(drivers);
            Assert.Empty(drivers);
        }
    }
}