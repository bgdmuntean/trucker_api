﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Faker;
using Microsoft.EntityFrameworkCore;
using Trucker.API.Core.Data;
using Trucker.API.Domain.DTOs;
using Trucker.API.Repositories.Drivers;
using Xunit;
using Name = Faker.Name;

namespace Trucker.API.Integration.Infrastructure.Driver
{
    public class GetDriversOverGivenAgeTests
    {
        [Fact]
        public async Task DriverRepository_CanReturnDriversOverAge_WhenThereAreDriversOverAge()
        {
            // Arrange 

            var options = new DbContextOptionsBuilder<TruckerDbContext>()
                    .UseInMemoryDatabase("TruckerStorage")
                    .Options;

            await using (var context = new TruckerDbContext(options))
            {
                await context.Drivers.AddAsync(new DriverDto
                {
                    Id = Guid.NewGuid(),
                    FirstName = Name.FirstName(),
                    LastName = Name.LastName(),
                    Nationality = "German",
                    DriversLicenseNo = Lorem.Word(),
                    Birthdate = DateTime.Now.AddYears(-30)
                });

                await context.Drivers.AddAsync(new DriverDto
                {
                    Id = Guid.NewGuid(),
                    FirstName = Name.FirstName(),
                    LastName = Name.LastName(),
                    Nationality = "German",
                    DriversLicenseNo = Lorem.Word(),
                    Birthdate = Date.Between(DateTime.Now.AddYears(-30), DateTime.Now.AddYears(-21))
                });

                await context.SaveChangesAsync();
            }


            await using (var context = new TruckerDbContext(options))
            {
                // Act

                var sut = new DriverRepository(context);

                var drivers = await sut.GetDriversOverGivenAgeAsync(29);

                // Assert

                Assert.IsAssignableFrom<ICollection<Domain.Models.Driver>>(drivers);
                Assert.NotNull(drivers);
                Assert.NotEmpty(drivers);
            }
        }

        [Fact]
        public async Task DriverRepository_CanReturnEmptyList_WhenThereAreNoDriversOverAge()
        {
            // Arrange 

            var options = new DbContextOptionsBuilder<TruckerDbContext>()
                .UseInMemoryDatabase("TruckerStorage")
                .Options;

            await using (var context = new TruckerDbContext(options))
            {
                await context.Drivers.AddAsync(new DriverDto
                {
                    Id = Guid.NewGuid(),
                    FirstName = Name.FirstName(),
                    LastName = Name.LastName(),
                    Nationality = "German",
                    DriversLicenseNo = Lorem.Word(),
                    Birthdate = DateTime.Now.AddYears(-30)
                });

                await context.Drivers.AddAsync(new DriverDto
                {
                    Id = Guid.NewGuid(),
                    FirstName = Name.FirstName(),
                    LastName = Name.LastName(),
                    Nationality = "German",
                    DriversLicenseNo = Lorem.Word(),
                    Birthdate = Date.Between(DateTime.Now.AddYears(-30), DateTime.Now.AddYears(-21))
                });

                await context.SaveChangesAsync();
            }


            await using (var context = new TruckerDbContext(options))
            {
                // Act

                var sut = new DriverRepository(context);

                var drivers = await sut.GetDriversOverGivenAgeAsync(70);

                // Assert

                Assert.IsAssignableFrom<ICollection<Domain.Models.Driver>>(drivers);
                Assert.NotNull(drivers);
                Assert.Empty(drivers);
            }
        }
    }
}