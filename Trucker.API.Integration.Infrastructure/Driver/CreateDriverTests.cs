﻿using System;
using System.Threading.Tasks;
using Faker;
using Microsoft.EntityFrameworkCore;
using Trucker.API.Core.Data;
using Trucker.API.Repositories.Drivers;
using Xunit;
using Name = Trucker.API.Domain.Models.ValueObjects.Driver.Name;

namespace Trucker.API.Integration.Infrastructure.Driver
{
    public class CreateDriverTests
    {
        private readonly IDriverRepository _sut;
        public CreateDriverTests()
        {
            var options = new DbContextOptionsBuilder<TruckerDbContext>()
                .UseInMemoryDatabase(databaseName: "TruckerStorage")
                .Options;
            _sut = new DriverRepository(new TruckerDbContext(options));
        }

        [Fact]
        public async Task DriverRepository_CanCreateDriver_WhenAllDataIsCorrect()
        {
            // Arrange

            var firstName = Faker.Name.FirstName();
            var lastName = Faker.Name.LastName();
            var nationality = "German";
            var driversLicenseNo = Lorem.Word();
            var birthdate = Date.Between(DateTime.Now.AddYears(-30), DateTime.Now.AddYears(-21));

            // Act

            var driver = await _sut.CreateDriverAsync(firstName, lastName, birthdate, nationality, driversLicenseNo);

            // Assert

            Assert.IsType<Domain.Models.Driver>(driver);
            Assert.NotNull(driver);
            Assert.Equal(Name.From((firstName, lastName)), driver.Name);
        }

        // No other tests are needed as there isn't any validation done on this side.
    }
}