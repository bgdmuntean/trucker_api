﻿using Trucker.API.Domain.Exceptions.ValueObjectExceptions;
using Trucker.API.Domain.Models.ValueObjects.Driver;
using Xunit;

namespace Trucker.API.UnitTests.Domain.Driver
{
    public class NameValueObjectTests
    {
        [Fact]
        public void NameValueObject_ShouldSucceed_WhenNameIsValid()
        {
            // Arrange

            var firstName = Faker.Name.MaleFirstName();
            var lastName = Faker.Name.LastName();

            // Act

            var nameValueObject = Name.From((firstName, lastName));

            // Assert

            Assert.NotNull(nameValueObject);
        }

        [Fact]
        public void NameValueObject_ShouldThrowIllegalNameException_WhenNameIsNotProvided()
        {
            // Arrange 

            // Act && Assert

            Assert.Throws<IllegalNameException>(() => { Name.From((null, null)); });
        }

        [Fact]
        public void NameValueObject_ShouldThrowIllegalNameException_WhenFirstNameIsNotProvided()
        {
            // Arrange 

            var lastName = Faker.Name.LastName();

            // Act && Assert

            Assert.Throws<IllegalNameException>(() => { Name.From((null, lastName)); });
        }

        [Fact]
        public void NameValueObject_ShouldThrowIllegalNameException_WhenLastNameIsNotProvided()
        {
            // Arrange 

            var firstName = Faker.Name.MaleFirstName();

            // Act && Assert

            Assert.Throws<IllegalNameException>(() => { Name.From((firstName, null)); });
        }
    }
}
