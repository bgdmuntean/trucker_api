﻿using Trucker.API.Domain.Exceptions.ValueObjectExceptions;
using Trucker.API.Domain.Models.ValueObjects.Driver;
using Xunit;

namespace Trucker.API.UnitTests.Domain.Driver
{
    public class NationalityValueObjectTests
    {
        [Fact]
        public void NationalityValueObject_ShouldSucceed_WhenNationalityIsValid()
        {
            // Arrange

            var nationality = "Danish";

            // Act

            var nationalityValueObject = Nationality.From(nationality);

            // Assert

            Assert.NotNull(nationalityValueObject);
        }

        [Fact]
        public void NationalityValueObject_ShouldThrowUnknownNationalityException_WhenNationalityIsInvalid()
        {
            // Arrange

            var nationality = "Something else";

            // Act && Assert

            Assert.Throws<UnknownNationalityException>(() => { Nationality.From(nationality); });
        }


        [Fact]
        public void NationalityValueObject_ShouldThrowNullOrEmptyNationalityException_WhenNationalityIsNull()
        {
            // Arrange
            // Act && Assert

            Assert.Throws<NullOrEmptyNationalityException>(() => { Nationality.From(null); });
        }

        [Fact]
        public void NationalityValueObject_ShouldThrowNullOrEmptyNationalityException_WhenNationalityIsEmpty()
        {
            // Arrange
            // Act && Assert

            Assert.Throws<NullOrEmptyNationalityException>(() => { Nationality.From(""); });
        }
    }
}