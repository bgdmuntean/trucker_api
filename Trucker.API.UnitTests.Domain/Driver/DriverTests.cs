﻿using System;
using Trucker.API.Domain.Models.ValueObjects.Driver;
using Xunit;

namespace Trucker.API.UnitTests.Domain.Driver
{
    public class DriverTests
    {
        [Fact]
        public void Driver_ShouldSucceed_WhenAllDataIsCorrect()
        {
            // Arrange
            var name = Name.From((Faker.Name.FirstName(), Faker.Name.LastName()));
            var id = Guid.NewGuid();
            var birthdate = Birthdate.From(DateTime.Now.AddYears(-34));
            var nationality = Nationality.From("German");
            var driversLicenseNo = "test"; 

            // Act

            var driver = new API.Domain.Models.Driver
            {
                Id = id,
                Name = name,
                Birthdate = birthdate,
                DriversLicenseNo = driversLicenseNo,
                Nationality = nationality,
                CreatedAt = DateTime.Now,
                ModifiedAt = DateTime.Now
            };

            // Assert

            Assert.NotNull(driver);
            Assert.Equal(name, driver.Name);
            Assert.Equal(id, driver.Id);
            Assert.Equal(birthdate, driver.Birthdate);
            Assert.Equal(nationality, driver.Nationality);
            Assert.Equal(driversLicenseNo, driver.DriversLicenseNo);
        }
    }
}