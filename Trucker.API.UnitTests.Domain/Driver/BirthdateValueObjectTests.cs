﻿using System;
using Trucker.API.Domain.Exceptions.ValueObjectExceptions;
using Trucker.API.Domain.Models.ValueObjects.Driver;
using Xunit;

namespace Trucker.API.UnitTests.Domain.Driver
{
    public class BirthdateValueObjectTests
    {
        
        [Fact]
        public void BirthdateValueObject_ShouldSucceed_WhenDateIsOver21()
        {
            // Arrange

            var birthdate = Faker.Date.Between(DateTime.Now.AddYears(-50), DateTime.Now.AddYears(-25));

            // Act

            var nameValueObject = Birthdate.From(birthdate);

            // Assert

            Assert.NotNull(nameValueObject);
        }

        [Fact]
        public void BirthdateValueObject_ShouldThrowMinimumDriverAgeException_WhenDateBelow21()
        {
            // Arrange

            var birthdate = Faker.Date.Between(DateTime.Now.AddYears(-20), DateTime.Now.AddYears(-18));

            // Act && Assert

            Assert.Throws<MinimumDriverAgeException>(() => { Birthdate.From(birthdate); });
        }
    }
}