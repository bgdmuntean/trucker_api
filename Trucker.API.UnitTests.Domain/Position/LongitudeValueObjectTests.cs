﻿using Trucker.API.Domain.Exceptions.ValueObjectExceptions;
using Trucker.API.Domain.Models.ValueObjects.Position;
using Xunit;

namespace Trucker.API.UnitTests.Domain.Position
{
    public class LongitudeValueObjectTests
    {
        [Fact]
        public void LongitudeValueObject_ShouldSucceed_WhenLongitudeIsValid()
        {

            // Arrange & Act

            var longitudeValueObject = Longitude.From(45M);

            // Assert

            Assert.NotNull(longitudeValueObject);
            Assert.Equal(Longitude.From(45M), longitudeValueObject);
        }

        [Fact]
        public void LongitudeValueObject_ShouldSucceed_WhenValueObjectIsTheLimit()
        {

            // Arrange & Act

            var longitudeValueObjectA = Longitude.From(180M);
            var longitudeValueObjectB = Longitude.From(-180M);

            // Assert

            Assert.NotNull(longitudeValueObjectA);
            Assert.Equal(Longitude.From(180M), longitudeValueObjectA);

            Assert.NotNull(longitudeValueObjectB);
            Assert.Equal(Longitude.From(-180M), longitudeValueObjectB);
        }


        [Fact]
        public void LongitudeValueObject_ShouldThrowIllegalLongitudeException_WhenLongitudeIsInvalid()
        {
            // Arrange & Act && Assert

            Assert.Throws<IllegalLongitudeException>(() => { Longitude.From(-200); });
            Assert.Throws<IllegalLongitudeException>(() => { Longitude.From(330); });
        }
    }
}