﻿using Trucker.API.Domain.Exceptions.ValueObjectExceptions;
using Trucker.API.Domain.Models.ValueObjects.Position;
using Trucker.API.Domain.Models.ValueObjects.TruckPlan;
using Xunit;

namespace Trucker.API.UnitTests.Domain.Position
{
    public class LatitudeValueObjectTests
    {
        [Fact]
        public void LatitudeValueObject_ShouldSucceed_WhenLatitudeIsValid()
        {

            // Arrange & Act

            var latitudeValueObject = Latitude.From(45M);

            // Assert

            Assert.NotNull(latitudeValueObject);
            Assert.Equal(Latitude.From(45M), latitudeValueObject);
        }

        [Fact]
        public void LatitudeValueObject_ShouldSucceed_WhenValueObjectIsTheLimit()
        {

            // Arrange & Act

            var latitudeValueObjectA = Latitude.From(90M);
            var latitudeValueObjectB = Latitude.From(-90M);

            // Assert

            Assert.NotNull(latitudeValueObjectA);
            Assert.Equal(Latitude.From(90M), latitudeValueObjectA);

            Assert.NotNull(latitudeValueObjectB);
            Assert.Equal(Latitude.From(-90M), latitudeValueObjectB);
        }


        [Fact]
        public void LatitudeValueObject_ShouldThrowIllegalLatitudeException_WhenLatitudeIsInvalid()
        {
            // Arrange & Act && Assert

            Assert.Throws<IllegalLatitudeException>(() => { Latitude.From(-111M); });
            Assert.Throws<IllegalLatitudeException>(() => { Latitude.From(111M); });
        }
    }
}