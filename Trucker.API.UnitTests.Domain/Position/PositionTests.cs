﻿using System;
using Trucker.API.Domain.Models.ValueObjects.Position;
using Xunit;

namespace Trucker.API.UnitTests.Domain.Position
{
    public class PositionTests
    {
        [Fact]
        public void Position_ShouldSucceed_WhenAllDataIsCorrect()
        {
            // Arrange
            var id = Guid.NewGuid();
            var truckPlanId = Guid.NewGuid();
            var timespan = Faker.Date.Recent(3);
            var country = Faker.Address.Country();
            var continent = "Europe";
            var latitude = Latitude.From(22M);
            var longitude = Longitude.From(33M);

            // Act

            var position = new API.Domain.Models.Position
            {
                Id = id,
                TruckPlanId = truckPlanId,
                Timespan = timespan,
                Country = country,
                Continent = continent,
                Latitude = latitude,
                Longitude = longitude
            };

            // Assert

            Assert.NotNull(position);
            Assert.Equal(id, position.Id);
            Assert.Equal(truckPlanId, position.TruckPlanId);
            Assert.Equal(timespan, position.Timespan);
            Assert.Equal(country, position.Country);
            Assert.Equal(continent, position.Continent);
            Assert.Equal(latitude, position.Latitude);
            Assert.Equal(longitude, position.Longitude);
        }
    }
}