﻿using System;
using Trucker.API.Domain.Models.ValueObjects.Driver;
using Trucker.API.Domain.Models.ValueObjects.TruckPlan;
using Xunit;

namespace Trucker.API.UnitTests.Domain.TruckPlan
{
    public class TruckPlanTests
    {
        [Fact]
        public void TruckPlan_ShouldSucceed_WhenAllDataIsCorrect()
        {
            // Arrange
            var id = Guid.NewGuid();
            var driverId = Guid.NewGuid();
            var start = Faker.Address.Country();
            var finish = Faker.Address.Country();
            var duration = Duration.From(Faker.Number.RandomNumber(0,123));
            var kilometers = Kilometers.From(Faker.Number.RandomNumber(0,123));

            // Act

            var truckPlan = new API.Domain.Models.TruckPlan
            {
                Id = id,
                DriverId = driverId,
                Start = start,
                Finish = finish,
                Duration = duration,
                Kilometers = kilometers
            };

            // Assert

            Assert.NotNull(truckPlan);
            Assert.Equal(id, truckPlan.Id);
            Assert.Equal(driverId, truckPlan.DriverId);
            Assert.Equal(start, truckPlan.Start);
            Assert.Equal(finish, truckPlan.Finish);
            Assert.Equal(duration, truckPlan.Duration);
            Assert.Equal(kilometers, truckPlan.Kilometers);
        }
    }
}