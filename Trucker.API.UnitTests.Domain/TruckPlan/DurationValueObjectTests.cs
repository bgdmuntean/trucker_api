﻿using Trucker.API.Domain.Exceptions.ValueObjectExceptions;
using Trucker.API.Domain.Models.ValueObjects.TruckPlan;
using Xunit;

namespace Trucker.API.UnitTests.Domain.TruckPlan
{
    public class DurationValueObjectTests
    {
        [Fact]
        public void DurationValueObject_ShouldSucceed_WhenDurationIsValid()
        {

            // Arrange & Act

            var durationValueObject = Duration.From(123.44M);

            // Assert

            Assert.NotNull(durationValueObject);
            Assert.Equal(Duration.From(123.44M), durationValueObject);
        }

        [Fact]
        public void DurationValueObject_ShouldSucceed_WhenDurationIsZero()
        {

            // Arrange & Act

            var durationValueObject = Duration.From(0M);

            // Assert

            Assert.NotNull(durationValueObject);
            Assert.Equal(Duration.From(0M), durationValueObject);
        }


        [Fact]
        public void DurationValueObject_ShouldThrowMinimumDurationException_WhenDurationIsNegative()
        {
            // Arrange & Act && Assert

            Assert.Throws<MinimumDurationException>(() => { Duration.From(-111M); });
        }
    }
}