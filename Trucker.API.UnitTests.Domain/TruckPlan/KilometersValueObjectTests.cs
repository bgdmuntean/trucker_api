﻿using Trucker.API.Domain.Exceptions.ValueObjectExceptions;
using Trucker.API.Domain.Models.ValueObjects.TruckPlan;
using Xunit;

namespace Trucker.API.UnitTests.Domain.TruckPlan
{
    public class KilometersValueObjectTests
    {
        [Fact]
        public void KilometersValueObject_ShouldSucceed_WhenDistanceIsValid()
        {

            // Arrange & Act

            var distanceValueObject = Kilometers.From(123.44M);

            // Assert

            Assert.NotNull(distanceValueObject);
            Assert.Equal(Kilometers.From(123.44M), distanceValueObject);
        }

        [Fact]
        public void KilometersValueObject_ShouldSucceed_WhenDistanceIsZero()
        {

            // Arrange & Act

            var distanceValueObject = Kilometers.From(0M);

            // Assert

            Assert.NotNull(distanceValueObject);
            Assert.Equal(Kilometers.From(0M), distanceValueObject);
        }


        [Fact]
        public void KilometersValueObject_ShouldThrowMinimumDistanceException_WhenDistanceIsNegative()
        {
            // Arrange & Act && Assert

            Assert.Throws<MinimumDistanceException>(() => { Kilometers.From(-111M); });
        }
    }
}